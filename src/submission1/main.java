package submission1;
import java.util.Scanner;
import tokoGame.game;

public class main {

    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        game objToko = new game();
        
        int pilih;
        
        keluar:
        while (true){
            System.out.println("======= Menu Data Game =======");
            System.out.println("1. Tambah data");
            System.out.println("2. Lihat data");
            System.out.println("3. Ubah data");
            System.out.println("4. Hapus data");
            System.out.println("5. Cari data");
            System.out.println("0. Keluar");
            System.out.print("Pilih menu : ");
            pilih = input.nextInt();
            switch(pilih){
                case 1 :
                    objToko.create();
                    break;
                case 2 :
                    objToko.show();
                    break;
                case 3 :
                    objToko.update();
                    break;
                case 4 :
                    objToko.delete();
                    break;
                case 5 :
                    objToko.search();
                    break;
                case 0 :
                    break keluar;
                default :
                    System.out.println("\n Pilihan salah! \n");
                    break;
            }
            
        }
    }
    
}
