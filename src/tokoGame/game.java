package tokoGame;

import java.util.ArrayList;
import java.util.Scanner;

public class game{
    
    private String code;
    private String judul;
    private float rating;
    private String tglRilis;
    private int harga;
    
    Scanner input = new Scanner(System.in);
    
    public void setCode(String code) {
        this.code = code;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setTglRilis(String tglRilis) {
        this.tglRilis = tglRilis;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getCode() {
        return code;
    }
    
    public String getJudul() {
        return judul;
    }

    public float getRating() {
        return rating;
    }

    public String getTglRilis() {
        return tglRilis;
    }

    public int getHarga() {
        return harga;
    }
    
    String genre;
    ArrayList <genre> arrGenre = new ArrayList<>();
    public void create(){
        int a = arrGenre.size();
        System.out.println("======= Menambah Data =======");
        System.out.print("Masukan genre Game    : ");
        genre = input.nextLine();
        arrGenre.add(new genre (genre));
        System.out.print("Masukan Jumlah Game : ");
        int jumlah = input.nextInt();
        input.nextLine();
        for (int i = 0; i < jumlah; i++) {
            game objGame = new game();
            System.out.print("Masukan kode Game    : ");
            code = input.nextLine();

            System.out.print("Masukan judul Game    : ");
            judul = input.nextLine();

            //input.nextLine();
            System.out.print("Masukan rating        : ");
            rating = input.nextFloat();

            input.nextLine();
            System.out.print("Masukan Tanggal Rilis : ");
            String tgl = input.nextLine();

            System.out.print("Masukan Harga         : Rp.");
            harga = input.nextInt();
            System.out.println("\n");

            objGame.setCode(code);
            objGame.setJudul(judul);
            input.nextLine();
            objGame.setRating(rating);
            objGame.setTglRilis(tgl);
            objGame.setHarga(harga);

            arrGenre.get(a).arrGame.add(objGame);
        }
        
    }
    
    public void show(){
        System.out.println("======= Melihat Data =======");
        for(int i = 0 ; i < arrGenre.size(); i++){
            System.out.println("\nGenre : " + arrGenre.get(i).getGenre());
            int gameSize = arrGenre.get(i).arrGame.size();
            for (int j = 0; j < gameSize; j++) {
                System.out.println("\nData Game ke-"+ (j+1));
                System.out.println("Kode Game       : " + arrGenre.get(i).arrGame.get(j).getCode());
                System.out.println("Judul Game      : " + arrGenre.get(i).arrGame.get(j).getJudul());
                System.out.println("Rating          : " + arrGenre.get(i).arrGame.get(j).getRating());
                System.out.println("Tanggal Rilis   : " + arrGenre.get(i).arrGame.get(j).getTglRilis());
                System.out.println("Harga           : " + arrGenre.get(i).arrGame.get(j).getHarga());
            }
            
        } 
         
        
        
    }
    
    public void update(){
        String ubah;
        System.out.println("======= Mengubah Data =======");
        System.out.print("Masukan nama genre :");
        genre = input.nextLine();
        
        int check1=0, check2=0, pos1=0, pos2=0;
        int cari = arrGenre.size();
        
        while(check1 < cari){
            if(genre.equals(arrGenre.get(check1).getGenre())){
                System.out.print("Masukan judul yang ingin di ubah : ");
                ubah = input.nextLine();
                int cari2 = arrGenre.get(check1).arrGame.size();
                while(check2 < cari2){
                    if (ubah.equals(arrGenre.get(check1).arrGame.get(check2).getJudul())) {
                        game objGame = new game();
                        System.out.print("Masukan kode Game    : ");
                        code = input.nextLine();

                        System.out.print("Masukan judul Game    : ");
                        judul = input.nextLine();

                        System.out.print("Masukan rating        : ");
                        rating = input.nextFloat();

                        input.nextLine();
                        System.out.print("Masukan Tanggal Rilis : ");
                        String tgl = input.nextLine();

                        System.out.print("Masukan Harga         : Rp. ");
                        harga = input.nextInt();
                        System.out.println("\n");
                        
                        objGame.setCode(code);
                        objGame.setJudul(judul);
                        input.nextLine();
                        objGame.setRating(rating);
                        objGame.setTglRilis(tgl);
                        objGame.setHarga(harga);

                        arrGenre.get(check1).arrGame.set(check2, objGame);
                        pos2=1;
                    }
                    check2++;
                }
                if (pos2 == 0) {
                    System.out.println("Judul game tidak ada!");
                }
                pos1 = 1;
            }
            check1++;
        }
        if (pos1 == 0) {
            System.out.println("Genre tidak ada!");
        }  
    }
    
    public void delete(){
        String cari;
        System.out.println("======= Menghapus Data =======");
        System.out.print("Masukan nama genre :");
        genre = input.nextLine();
        
        int check1=0, check2=0, pos1=0, pos2=0;
        int indeks = arrGenre.size();
        Integer markDelete = null, markDelete2 = null;
        while(check1 < indeks){
            if (genre.equals(arrGenre.get(check1).getGenre())) {
                System.out.print("Masukan judul yang ingin dihapus : ");
                int indeks2 = arrGenre.get(check1).arrGame.size();
                cari = input.nextLine();
                while(check2 < indeks2){
                    if(cari.contains(arrGenre.get(check1).arrGame.get(check2).getJudul())){
                        arrGenre.get(check1).arrGame.remove(check2);
                        pos2=1;
                        break;
                    }
                    check2++;
                }
                if (pos2 == 0) {
                    System.out.println("Judul Game tidak ada!");
                }
                pos1=1;
            }
            check1++;
        }
        if (pos1 == 0) {
            System.out.println("Genre tidak ada!");
        }
    }
    
    public void search(){
        String cari;
        System.out.println("======= Mencari Data =======\n");
        System.out.print("Masukan nama genre :");
        genre = input.nextLine();
        
        int check1=0, check2=0, pos1=0, pos2=0;
        int indeks = arrGenre.size();
        
        while(check1 < indeks){
            if (genre.equals(arrGenre.get(check1).getGenre())) {
                System.out.print("Masukan judul yang ingin dicari : ");
                int indeks2 = arrGenre.get(check1).arrGame.size();
                cari = input.nextLine();
                while(check2 < indeks2){
                    if(cari.contains(arrGenre.get(check1).arrGame.get(check2).getJudul())){
                        System.out.println("Kode Game       : " + arrGenre.get(check1).arrGame.get(check2).getCode());
                        System.out.println("Judul Game      : " + arrGenre.get(check1).arrGame.get(check2).getJudul());
                        System.out.println("Rating          : " + arrGenre.get(check1).arrGame.get(check2).getRating());
                        System.out.println("Tanggal Rilis   : " + arrGenre.get(check1).arrGame.get(check2).getTglRilis());
                        System.out.println("Harga           : " + arrGenre.get(check1).arrGame.get(check2).getHarga());
                        pos2=1;
                        break;
                    }
                    check2++;
                }
                if (pos2 == 0) {
                    System.out.println("Judul Game tidak ada!");
                }
                pos1=1;
            }
            check1++;
        }
        if (pos1 == 0) {
            System.out.println("Genre tidak ada!");
        }
    }
    
}
